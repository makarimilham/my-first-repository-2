// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
var cut1 = pertama.substr(0,5);
var cut2 = pertama.substr(12,7);
var cut3 = kedua.substr(0,7);
var cut4 = kedua.substr(7,11);
var cut5 = cut4.toUpperCase();

console.log(cut1.concat(cut2,cut3,cut5));

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var strInt_1 = parseInt(kataPertama);
var strInt_2 = parseInt(kataKedua);
var strInt_3 = parseInt(kataKetiga);
var strInt_4 = parseInt(kataKeempat);

console.log((strInt_1 - strInt_4) * (strInt_2 + strInt_3));

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18) ;
var kataKeempat = kalimat.substring(19,24) ;
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);