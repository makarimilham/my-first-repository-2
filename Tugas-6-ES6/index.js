//soal 1

const luas = () =>
    L = panjang * lebar

const keliling = () => 
    K = 2 * (panjang + lebar)

  let panjang = 10;
  let lebar = 7;
   
  HasilLuas = luas(panjang, lebar); 
  HasilKeliling = keliling(panjang, lebar);

  console.log(HasilLuas);
  console.log(HasilKeliling)


//soal 2

const newFunction = (firstName, lastName)=> {
  return {

    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
newFunction("William", "Imoh").fullName()

//soal 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)

//soal4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

//soal5

const planet = "earth" 
const view = "glass" 

const theString = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`

console.log(theString)