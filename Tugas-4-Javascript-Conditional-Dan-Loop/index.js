// soal 1
var nilai = 50;

if (nilai >= 85) {
    console.log("indeksnya A")
} else if (nilai >= 75 && nilai <85) {
    console.log("indeksnya B")
} else if (nilai >= 65 && nilai <75) {
    console.log("indeksnya C")
} else if (nilai >= 55 && nilai <65) {
    console.log("indeksnya D")
} else {
    console.log("indeksnya E")
}
 
// soal 2

var tanggal = 10;
var bulan = 6;
var tahun = 1995;

switch (bulan) {
    case 1: {console.log((tanggal) + " Januari " + (tahun)); break;}
    case 2: {console.log((tanggal) + " Februari " + (tahun)); break;}
    case 3: {console.log((tanggal) + " Maret " + (tahun)); break;}
    case 4: {console.log((tanggal) + " April " + (tahun)); break;}
    case 5: {console.log((tanggal) + " Mei " + (tahun)); break;}
    case 6: {console.log((tanggal) + " Juni " + (tahun)); break;}
    case 7: {console.log((tanggal) + " Juli " + (tahun)); break;}
    case 8: {console.log((tanggal) + " Agustus " + (tahun)); break;}
    case 9: {console.log((tanggal) + " September " + (tahun)); break;}
    case 10: {console.log((tanggal) + " Oktober " + (tahun)); break;}
    case 11: {console.log((tanggal) + " November " + (tahun)); break;}
    case 12: {console.log((tanggal) + " Desember " + (tahun)); break;}
    default : {console.log((tanggal) + " Tidak Ada " + (tahun)); break;}
}


// soal 3

var s = '';
var n = 7;

for(var a = 1; a <= n; a++) {
    for(var b = 1; b <= a; b++){
        s += '#';
  } 
  s += '\n'
}
console.log(s)

// soal 4

var m = 12
var a = " - I love Programming"
var b = " - I love Javascript"
var c = " - I love VueJS"
var d = ""


for(var h = 1;  h<=m; h ++) {
      
    if (h % 3 === 1){
        console.log(h + a);
    }    
     else if (h % 3 === 2){
        console.log(h + b)
    }
     else if (h % 3 === 0){
        d+= '==='
        console.log (h + c)
        console.log(d)
    }
}