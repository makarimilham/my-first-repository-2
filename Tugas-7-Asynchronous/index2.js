 var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
function baca(time, books, i){
    if (i<books.length){
        readBooksPromise(time, books[i], function(sisa){
            if (sisa > 0){
                i+=1;
                baca(sisa, books);
            }
        })
    }
}
readBooksPromise(10000, books [0])
    .then((response) => {
        return readBooksPromise(7000, books[1])
    })
    .then((response1) => {
        return readBooksPromise(5000, books[2])
    })
    .then((response2) => {
        return readBooksPromise(1000, books[3])
    })
    .catch(error => console.log(error))