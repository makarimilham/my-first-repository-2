// soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
    for (var i = 0; i < daftarHewan.length; i++)

console.log(daftarHewan[i])


// soal 2

function introduce(data) {
    var nama;
    var umur;
    var alamat;
    var hobby;
    var hasil;

    nama = "Nama saya " + data.name
    umur = ", umur saya " + data.age + " tahun"
    alamat = ", alamat saya di " + data.address
    hobby = ", dan saya punya hobby yaitu " + data.hobby
    hasil = nama + umur + alamat + hobby
    
    return hasil
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" } 
var perkenalan = introduce(data)

console.log(perkenalan)


// soal 3

function hitung_huruf_vokal(string) {

    var alfabet = ["a", "e", "i", "o", "u"]
    var jumlah = 0;

    for (var letter of string.toLowerCase()) {
        if (alfabet.includes(letter)) {
            jumlah++;
        }
    }

    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Ilham")
var hitung_2 = hitung_huruf_vokal("Makarim")

console.log(hitung_1, hitung_2);


// soal 4

function hitung(integer){
  
    
    for (var i = 0; i <= integer; i++)
        hasil = (integer * 2) - 2

    return hasil
}

console.log( hitung(0) ) 
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) ) 
